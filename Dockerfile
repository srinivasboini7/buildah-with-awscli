FROM docker.io/centos:8

RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
RUN sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
RUN dnf -y distro-sync
RUN dnf -y install java
RUN dnf -y update
RUN echo "Install aws cli v2"
RUN dnf -y install unzip
RUN curl https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip
RUN unzip -q awscliv2.zip
RUN ./aws/install
RUN export PATH=/usr/local/bin:$PATH

RUN echo "Install buildah"
RUN dnf install -y podman buildah fuse-overlayfs
RUN sed -i 's/#mount_program/mount_program/' /etc/containers/storage.conf



# Set entry point
ENTRYPOINT ["/bin/bash", "-l", "-c"]